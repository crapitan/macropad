# Macropad hackathon

*"En del i varje Jedis utbildning är att konstruera ett lightsaber utifrån en kristall given av dess mästare. Resultatet blir ett unikt verktyg för den unge Jedien som speglar dess personlighet och styrka"* -  Holocron

## Sammanfattning
I det här hacket skall vi bygga ett enkelt tangenbord bestående av upp till 12 tangenter. Funktionerna för tangenbordet och layout kommer att vara helt upp till dig. Vi kommer att ha ett färdigt kit med instruktionern hur man bygger och flashar styrkrestsen, men det går även bra att bygga ett helt eget. Mjukvaran till styrkretsen har stöd för både lysdiodrar (Enkel/RGB) och eller beephögtalare (Om du vill spela lite enkel musik) vi har dock inga extra med oss.

När du är klar med hacket kommer du ha tillräkligt med kunskap för att kunna på egen hand bygga tangenbord.  
   

## Utrustning
Vi kommer att tillhandahålla ett kit med.

 * Switchar i olika varianter (färger) 
 * Diodrar, 
 * KeyCaps med olika färger, 
 * Plattor för montering, 
 * Styrkrets(Aurdino) 
 * Distanser.  

Det är bra om du kan ta med dig **lödutrustning** och en **miniusb sladd**  
